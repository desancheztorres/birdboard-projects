<?php

namespace App\Policies;

use App\{User, Project};
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the user may manage a project.
     *
     * @param  User    $user
     * @param  Project $project
     * @return bool
     */
    public function manage(User $user, Project $project)
    {
        return $user->is($project->owner);
    }

    public function update(User $user, Project $project) {
        return $user->is($project->owner) || $project->members->contains($user);
    }
}
