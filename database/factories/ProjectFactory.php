<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        // This data below becomes the dummy data used in the test cases!

        // To use factory dummmy data, call 'php artisan tinker && factory('App\Project')->create() or ->make()(make wont persists/save the data)' in the command line
        'title' => $faker->sentence(4),

        'description' => $faker->sentence(4),

        'notes' => 'Foobar notes',

        // Helper class to create dummy user and return the id
        'owner_id' => factory(App\User::class)
    ];
});
