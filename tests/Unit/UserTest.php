<?php

namespace Tests\Unit;

use App\User;
use Facades\Tests\Setup\ProjectFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_has_projects()
    {
        $user = factory('App\User')->create();

        $this->assertInstanceOf(Collection::class, $user->projects);
    }

    /** @test */
    public function a_user_has_accessible_projects()
    {
        $cristian = $this->signIn();
        ProjectFactory::ownedBy($cristian)->create();

        $this->assertCount(1, $cristian->accessibleProjects());

        $oscar = factory(User::class)->create();
        $javi = factory(User::class)->create();

        $project = tap(ProjectFactory::ownedBy($oscar)->create())->invite($javi);

        $this->assertCount(1, $cristian->accessibleProjects());

        $project->invite($cristian);

        $this->assertCount(2, $cristian->accessibleProjects());

    }
}
